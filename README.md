**Human Protein Atlas Kaggle Competition**

To:
* Predict various protein structures in cellular images
* there are 28 different target proteins
* multiple proteins can be present in one image (multilabel classification)
* 27 different cell types of highly different morphology
